<p align="center">
 <a target="_blank" href="https://gitee.com/llz888888/base-mall-prod-web"><strong style="font-size: 40px">基础商品管理平台</strong></a>  
</p>

## 项目介绍

本项目基于开源项目Ruoyi后台管理系统, 开发了供应商管理, 品牌管理, 类目管理, 商品管理等功能, 可以作为一套基础的商品管理系统, 快速二次开发成完整商城管理系统.

项目有以下特性：
- 基于 Vue2 版本;
- 采用了主流的 webpack + vuex 等技术栈;
- 项目还提供了基础设施支持，包括动态路由、按钮级别的权限控制, 以便开发人员更高效地开发和维护项目。

## 体验地址

账号: admin

密码: 123456

地址: [临时体验地址](https://93ff-220-241-42-160.ngrok-free.app)

注意: 该账号为管理员账号, 请别随意删除用户数据, 谢谢!

## 项目预览

- **类目列表**

  ![类目列表](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/category-list.png)


- **类目列表-关联品牌**

  ![类目列表-关联品牌](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/category-brand.png)


- **类目列表-关联属性**

  ![类目列表-关联属性](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/category-attribute.png)


- **品牌列表**

  ![品牌列表](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/brand-list.png)


- **品牌列表-编辑**

  ![品牌列表-编辑](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/brand-edit.png)


- **商品列表**

  ![商品列表](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/prod-list-1.png)


- **商品列表-查看sku信息**

  ![商品列表-查看sku信息](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/prod-list-2.png)

- **商品发布-step-1**

  ![商品发布](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/publish-prod-1-1.png)
  ![商品发布](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/publish-prod-1-2.png)


- **商品发布-step-2**
 
  ![商品发布](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/publish-prod-2.png)

- **商品发布-step-3**

  ![商品发布](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/publish-prod-3-1.png)
  ![商品发布](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/publish-prod-3-2.png)
  ![商品发布](https://gitee.com/llz888888/static/raw/master/base-mall-prod-web/readme/publish-prod-3-3.png)

## 项目地址

| 项目 | Gitee                                                                    |
|----|--------------------------------------------------------------------------| 
| 前端 | [base-mall-prod-web](https://gitee.com/llz888888/base-mall-prod-web.git) |

## 安装依赖, 启动服务

```bash
# 克隆代码
git clone https://gitee.com/llz888888/base-mall-prod-web.git

# 切换目录
cd base-mall-prod-web

# 查看node版本
node -v
# 切换到合适的node版本 10.x.x (使用nvm)
nvm list
# 如果没有14版本的node, 就通过NVM安装一个
nvm install 14.0.0
nvm use 14.0.0
node -v

# 安装依赖
npm install

# 启动运行
npm run dev
```


## 访问服务
浏览器访问 http://localhost

## 打包发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```