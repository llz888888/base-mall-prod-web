import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getToken } from '@/utils/auth'
import { isRelogin } from '@/utils/request'

NProgress.configure({ showSpinner: false })

const whiteList = ['/login', '/register'] //配置路径白名单放行

//路由全局前置守卫
router.beforeEach((to, from, next) => {
  NProgress.start()
  if (getToken()) {
    /* has token*/

    //更改title
    to.meta.title && store.dispatch('settings/setTitle', to.meta.title)

    if (to.path === '/login') { //token仍然存在的情况下, 强制不给跳到登录页(没必要再次登录)
      next({ path: '/' })
      NProgress.done()
    } else {
      if (store.getters.roles.length === 0) { //判断是否未初始化用户信息,权限菜单等
        isRelogin.show = true
        // 判断当前用户是否已拉取完user_info信息
        store.dispatch('GetInfo').then(() => {
          isRelogin.show = false
          // 根据roles权限生成可访问的路由表
          store.dispatch('GenerateRoutes').then(accessRoutes => {
            router.addRoutes(accessRoutes) // ***** 动态添加可访问路由表
            next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
          })
        }).catch(err => {
            store.dispatch('LogOut').then(() => {
              Message.error(err)
              next({ path: '/' })
            })
          })
      } else {
        next()
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      // 不在白名单, 也没有token, 就要强制跳到登录页 !
      next(`/login?redirect=${to.fullPath}`) // 否则全部重定向到登录页
      NProgress.done()
    }
  }
})

//路由全局后置守卫
router.afterEach(() => {
  NProgress.done()
})
