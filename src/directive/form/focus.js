
export default {
  // 当被绑定的元素插入到 DOM 中时……
  inserted(el, binding, vnode)  {
    // 聚焦元素
    // el.focus()
    el.querySelector('input').focus()
  },
  update(el, binding)  {
    // 聚焦元素
    // el.focus()
    el.querySelector('input').focus()
  }
}
