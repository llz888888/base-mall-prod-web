import request from '@/utils/request';

export function listBrand(query) {
  return request({
    url: '/prod/brand/list',
    method: 'get',
    params: query
  })
}

export function addBrand(data) {
  return request({
    url: '/prod/brand',
    method: 'post',
    data: data
  })
}

export function deleteBrand(id) {
  return request({
    url: '/prod/brand/' + id,
    method: 'delete'
  })
}

export function editBrand(id) {
  return request({
    url: '/prod/brand/' + id,
    method: 'get'
  })
}

export function updateBrand(data) {
  return request({
    url: '/prod/brand/',
    method: 'put',
    data: data
  })
}

export function checkExisting(pathParamsStr) {
  return request({
    url: '/prod/brand/check/existing/' + pathParamsStr,
    method: 'get',
  })
}
