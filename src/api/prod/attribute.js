import request from '@/utils/request'

// 查询属性名列表
export function listAttribute(query) {
  return request({
    url: '/prod/attribute/list',
    method: 'get',
    params: query
  })
}

// 查询属性名详细
export function getAttribute(id) {
  return request({
    url: '/prod/attribute/' + id,
    method: 'get'
  })
}

// 新增属性名
export function addAttribute(data) {
  return request({
    url: '/prod/attribute',
    method: 'post',
    data: data
  })
}

// 修改属性名
export function updateAttribute(data) {
  return request({
    url: '/prod/attribute',
    method: 'put',
    data: data
  })
}

// 删除属性名
export function delAttribute(id) {
  return request({
    url: '/prod/attribute/' + id,
    method: 'delete'
  })
}
