import request from '@/utils/request'

// 查询类目列表
export function listCategory(query) {
  return request({
    url: '/prod/category/list',
    method: 'get',
    params: query
  })
}

// 查询类目详细

export function getCategory(id) {
  return request({
    url: '/prod/category/' + id,
    method: 'get'
  })
}

// 新增类目
export function addCategory(data) {
  return request({
    url: '/prod/category',
    method: 'post',
    data: data
  })
}

// 修改类目
export function updateCategory(data) {
  return request({
    url: '/prod/category',
    method: 'put',
    data: data
  })
}

// 删除类目
export function delCategory(id) {
  return request({
    url: '/prod/category/' + id,
    method: 'delete'
  })
}

//GET /prod/category/list
export function getCategoryList() {
  return request({
    url: '/prod/category/list',
    method: 'get'
  })
}

// GET /prod/relate/category/brands/:categoryId
// 获取品牌关联
export function getBrand(categoryId) {
  return request({
    url: '/prod/relate/category/brands/' + categoryId,
    method: 'get'
  })
}

//获取所有品牌
// GET /prod/brand/list
export function getBrandsByName(data) {
  return request({
    url: '/prod/brand/list',
    method: 'get',
    params: data
  })
}

// 品牌关联
// POST /prod/relate/category/brands
export function updateBrand(data) {
  return request({
    url: '/prod/relate/category/brands',
    method: 'post',
    data: data
  })
}
