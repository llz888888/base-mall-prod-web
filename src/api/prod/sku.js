import request from '@/utils/request'

// 查询商品管理列表
export function querySkuBasedOnId(query) {
  return request({
    url: '/prod/sku/list',
    method: 'get',
    params: query
  })
}

//库存变量的修改
export function updateInventoryVariables(data) {
  return request({
    url: '/prod/sku/',
    method: 'put',
    data: data
  })
}

