import request from '@/utils/request'

// 查询商品管理列表
export function listProd(query) {
  return request({
    url: '/prod/prod/list',
    method: 'get',
    params: query
  })
}

// 查询商品管理详细
export function getProd(id) {
  return request({
    url: '/prod/prod/' + id,
    method: 'get'
  })
}

// 新增商品管理
export function addProd(data) {
  return request({
    url: '/prod/prod',
    method: 'post',
    data: data
  })
}

// 修改商品管理
export function updateProd(data) {
  return request({
    url: '/prod/prod',
    method: 'put',
    data: data
  })
}



// 删除商品管理
export function delProd(id) {
  return request({
    url: '/prod/prod/' + id,
    method: 'delete'
  })
}

//获取分类列表
// GET /prod/category/list
export function getCategoryList(query) {
  return request({
    url: '/prod/category/list',
    method: 'get',
    params: query
  })
}

//获取分类列表
// PUT /prod/prod/status/{prodId}/{status}
export function modifyCommodityStatus(prodId, status) {
  let url = '/prod/prod/status/' + prodId + "/" + status;
  return request({
    url: url,
    method: 'put'
  })
}

//根据三级类目id, 反向获取到 二级, 一级类目的id数组;
export function getCategoryIdsByCategory3(categoryId3) {
  return request({
    url: '/prod/category/chain/' + categoryId3,
    method: 'get'
  })
}
