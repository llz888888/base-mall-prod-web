import request from '@/utils/request';


export function listFreightTemplate(query) {
  return request({
    url: '/prod/template/list',
    method: 'get',
    params: query
  })
}


