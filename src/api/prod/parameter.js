import request from '@/utils/request'

// 查询参数名列表
export function listParameter(query) {
  return request({
    url: '/prod/parameter/list',
    method: 'get',
    params: query
  })
}

// 查询参数名详细
export function getParameter(id) {
  return request({
    url: '/prod/parameter/' + id,
    method: 'get'
  })
}

// 新增参数名
export function addParameter(data) {
  return request({
    url: '/prod/parameter',
    method: 'post',
    data: data
  })
}

// 修改参数名
export function updateParameter(data) {
  return request({
    url: '/prod/parameter',
    method: 'put',
    data: data
  })
}

// 删除参数名
export function delParameter(id) {
  return request({
    url: '/prod/parameter/' + id,
    method: 'delete'
  })
}
