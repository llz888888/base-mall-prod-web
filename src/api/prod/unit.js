import request from '@/utils/request';

// 查询岗位列表
// GET /prod/unit/list
// Request parameter:
// pageNum=1
// pageSize=10
// name=""  //非必传, 搜索框条件查询时可传
export function listUnit(query) {
  return request({
    url: '/prod/unit/list',
    method: 'get',
    params: query
  })
}

// 修改单位
//GET /prod/unit/:id
export function getUnit(unitId) {
  return request({
    url: '/prod/unit/' + unitId,
    method: 'get',
  })
}


// 提交修改单位
//PUT /prod/unit
// Request body:
// {
//   "id":   int,
//   "name": string
// }
export function updateUnit(data) {
  return request({
    url: '/prod/unit/',
    method: 'put',
    data: data
  })
}


// 删除单位
//DELETE /prod/unit/:id
export function deleteUnit(unitId) {
  return request({
    url: '/prod/unit/' + unitId,
    method: 'delete',

  })
}

// 添加
// POST /prod/unit
// Request body:
// {
//   "id": null,
//   "name": string
// }
export function addUnit (data) {
  return request({
    url: '/prod/unit/',
    method: 'post',
    data: data

  })
}

// 校验
//GET /prod/unit/check/existing/:name/:id
export function checkExisting (pathParamsStr) {
  return request({
    url: '/prod/unit/check/existing/' + pathParamsStr,
    method: 'get',
  })
}


