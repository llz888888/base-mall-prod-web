import item from "@/layout/components/Sidebar/Item";

export function calSkuCombination(totalProps) {

  // let colors = ["白色", "黑色"];
  // let cpuMems = ["8G", "12G"];
  // let dataMems = ["128G", "256G", "512G"];
  // let totalProps = [colors, cpuMems, dataMems];
 if (totalProps === undefined || totalProps === null || totalProps.length === 0){
   return [];
 }

 if (totalProps.length === 1){
   return totalProps[0].map(item => {
             let arr = [];
             arr.push(item);
             return arr;
           })
 }

  let skuCombinations = totalProps.reduce((current, next) =>{
    let results = [];
    current.forEach(a =>{
      next.forEach(b =>{
        let arr = [];
        if (a instanceof Array){
          arr = [...a];
          arr.push(b);
        }else {
          arr.push(a);
          arr.push(b);
        }
        results.push(arr);
      })
    })
    return results;
  });

  return skuCombinations;
}
