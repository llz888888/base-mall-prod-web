
// 转换成树形结构
// 方法1
export function treeStructure(allCategoryList){
  let rootCategoriesList = allCategoryList.filter(Category => Category.parentId === 0);

  rootCategoriesList.forEach( rootCategory => findChildren(rootCategory, allCategoryList));
  return rootCategoriesList

  function findChildren(currentCategory, allCategoryList){
    let parentId = currentCategory.id;
    currentCategory.children = allCategoryList.filter(category => category.parentId === parentId);

    if( currentCategory.children.length > 0){
      currentCategory.children.forEach(childrenCategory => findChildren(childrenCategory,allCategoryList ))
    }
  }
}


// 方法2
// treeStructure(allCategoriesList){
//   //先找出一级分类列表 根据parentId === 0 来判断一级分类
//   let rootCategoriesList = [];
//   for(const categoryKey in allCategoriesList){
//     let category = allCategoriesList[categoryKey];
//     if(category.parentId === 0){
//       rootCategoriesList.push(category)
//     }
//   }
//
//   for(const rootCategoriesListKey in rootCategoriesList){
//     let rootCategory = rootCategoriesList[rootCategoriesListKey]
//     this.findChildren(rootCategory, allCategoriesList)
//   }
//   return rootCategoriesList
// },

// findChildren(currentCategory, allCategoriesList){
//   //找二级分类列表 根据一级id === 二级的parentId 来找到
//   let parentId = currentCategory.id;
//   let children = [];
//   for(const categoryKey in allCategoriesList){
//     let category = allCategoriesList[categoryKey]
//     if(category.parentId === parentId){
//       children.push(category)
//     }
//   }
//   currentCategory.children = children
//
//
//   if(currentCategory.children.length > 0){
//     for(const childrenCategoryKey in children){
//       let childrenCategory = children[childrenCategoryKey]
//       this.findChildren(childrenCategory, allCategoriesList)
//     }
//   }
// }
